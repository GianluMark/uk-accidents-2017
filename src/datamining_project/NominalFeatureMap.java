package datamining_project;

import java.util.ArrayList;

public class NominalFeatureMap {
    
    ArrayList<String> indexes;
    ArrayList<String> names;
    
    public NominalFeatureMap(){
        
        indexes = new ArrayList<>();
        names = new ArrayList<>();
    }
    
    public void addValue(String index, String name){
        indexes.add(index);
        names.add(name);
    }
    
    public String getIndexFromName(String name){
        
        String index = "";
        int listIndex = 0;
        
        for(int i = 0; i<names.size(); i++)
            if(names.get(i).equals(name))
                listIndex = i;
        
        index = indexes.get(listIndex);
        return index;
    }

    public ArrayList<String> getNames() {
        return names;
    }

    public ArrayList<String> getIndexes() {
        return indexes;
    }
    
    public int getPositionFromList(String value){
        int position = 0;
        for(int i = 0; i<indexes.size(); i++)
            if(indexes.get(i).equals(value))
                position = i;
        return position;
    }
}
