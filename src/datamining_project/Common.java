/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining_project;

/**
 *
 * @author gianl
 */
public class Common {
    
    // Directories
    public static String actualDirectory = System.getProperty("user.dir");
    public static String featuresDirectory = actualDirectory + "\\DATA\\features\\";
    public static String datasetDirectory = actualDirectory + "\\DATA\\dataset\\";
    public static String classifierDirectory = actualDirectory + "\\DATA\\classifier\\";
    
    // Files
    public static String datasetFile = datasetDirectory + "DATASET.arff";
    public static String policeForceFeatures = featuresDirectory + "policeForce.txt";
    public static String accidentSeverityFeatures = featuresDirectory + "accidentSeverity.txt";
    public static String dayOfWeekFeatures = featuresDirectory + "dayOfWeek.txt";
    public static String localAuthorityFeatures = featuresDirectory + "localAuthority.txt";
    public static String firstRoadClassFeatures = featuresDirectory + "firstRoadClass.txt";
    public static String speedLimitFeatures = featuresDirectory + "speedLimit.txt";
    public static String lightConditionsFeatures = featuresDirectory + "lightConditions.txt";
    public static String weatherConditionsFeatures = featuresDirectory + "weatherConditions.txt";
    public static String urbanRuralFeatures = featuresDirectory + "urbanRural.txt";
    public static String classifierModel = classifierDirectory + "normalizedClassifiers\\RandomForestFiltered.model";
    public static String longLatPoliceForceModel = classifierDirectory + "normalizedClassifiers\\LongLatPoliceForce_J48.model";
    public static String longLatLocalAuthorityModel = classifierDirectory + "normalizedClassifiers\\LongLatLocalAuthority_J48.model";
    public static String clusteringStatsFile = datasetDirectory + "clusteringInfo.csv";
    
    // Features
    public static int[] policeForceIndexValues = {1,3,4,5,6,7,10,11,12,13,14,16,17,20,21,22,23,30,31,32,33,34,35,36,37,40,41,42,43,44,45,46,47,48,50,52,53,54,55,60,61,62,63,91,92,93,94,95,96,97,98};
    public static int[] localAuthorityIndexValues = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,38,40,57,60,61,62,63,64,65,70,71,72,73,74,75,76,77,79,80,82,83,84,85,90,91,92,93,95,100,101,102,104,106,107,109,110,112,114,124,128,129,130,139,146,147,148,149,150,161,169,180,181,182,184,185,186,187,189,200,202,203,204,206,210,211,213,215,228,231,232,233,240,241,243,245,250,251,252,253,254,255,256,257,258,270,273,274,276,277,278,284,285,286,290,291,292,293,294,300,302,303,305,306,307,309,320,321,322,323,324,325,327,328,329,340,341,342,343,344,345,346,347,350,351,352,353,354,355,356,360,361,362,363,364,365,366,367,368,380,381,382,383,384,385,386,390,391,392,393,394,395,400,401,402,404,405,406,407,410,411,412,413,414,415,416,420,421,424,430,431,432,433,434,435,436,437,438,450,451,452,453,454,455,456,457,458,459,460,461,462,463,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,490,491,492,493,494,495,496,497,498,499,500,501,502,505,510,511,512,513,514,515,516,517,518,530,531,532,533,535,536,538,539,540,541,542,543,544,551,552,554,555,556,557,558,559,560,562,563,564,565,570,580,581,582,583,584,585,586,587,588,589,596,601,605,606,607,608,609,610,611,612,620,621,622,623,624,625,633,635,640,641,642,643,644,645,646,647,720,721,722,723,724,725,730,731,732,733,734,740,741,742,743,744,745,746,750,751,752,753,910,911,912,913,914,915,916,917,918,919,920,921,922,923,924,925,926,927,928,929,930,931,932,933,934,935,936,937,938,939,940,941};
    public static String yesValue = "1";
    public static String noValue = "2";
    public static int numberOfPoliceForces = 51;
    public static int numberOfLocalAuthorities = 380;
    public static int numberOfLightConditions = 5;
    public static int numberOfWeatherConditions = 7;
    
    // Ranges
    public static double longitudeMin = -7.41;
    public static double longitudeMax = 1.76;
    public static double latitudeMin = 49.93;
    public static double latitudeMax = 60.481;
    public static double accidentSeverityMin = 0.0;
    public static double accidentSeverityMax = 2.0;
    public static double dayOfWeekMin = 0.0;
    public static double dayOfWeekMax = 6.0;
    public static double firstRoadClassMin = 0.0;
    public static double firstRoadClassMax = 5.0;
    public static double speedLimitMin = 0.0;
    public static double speedLimitMax = 5.0;
    public static double newMin = 0.0;
    public static double newMax = 1.0;
}
