/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining_project;

import java.util.ArrayList;
import java.util.List;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

public class Dataset {

	// Attributes
	Attribute longitude;
	Attribute latitude;
	Attribute police_force;
	Attribute accident_severity;
	Attribute day_of_week;
	Attribute local_authority;
	Attribute first_road_class;
	Attribute speed_limit;
	Attribute light_conditions;
	Attribute weather_conditions;
	Attribute urban_rural;
	
	Attribute police_officer_attend;

	// ArrayLists
	ArrayList<String> policeForces;
	ArrayList<String> accidentSeverities;
	ArrayList<String> daysOfWeek;
	ArrayList<String> localAuthorities;
	ArrayList<String> firstRoadClasses;
	ArrayList<String> speedLimits;
	ArrayList<String> lightConditions;
	ArrayList<String> weatherConditions;
	ArrayList<String> urbanRurals;
	
	ArrayList<String> policeOfficerAttends;

	// Dataset
	Instances dataset;

	public Dataset() {}

	public void initializeDataset(ArrayList<String> policeForces, ArrayList<String> accidentSeverities, ArrayList<String> daysOfWeek, ArrayList<String> localAuthorities, ArrayList<String> firstRoadClasses, ArrayList<String> speedLimits, ArrayList<String> lightConditions, ArrayList<String> weatherConditions, ArrayList<String> urbanRurals, ArrayList<String> policeOfficerAttends) {

		// Initialize lists
		
		this.policeForces = policeForces;
		this.accidentSeverities = accidentSeverities;
		this.daysOfWeek = daysOfWeek;
		this.localAuthorities = localAuthorities;
		this.firstRoadClasses = firstRoadClasses;
		this.speedLimits = speedLimits;
		this.lightConditions = lightConditions;
		this.weatherConditions = weatherConditions;
		this.urbanRurals = urbanRurals;
		this.policeOfficerAttends = policeOfficerAttends;

		// Initialize numeric and date attributes
		longitude = new Attribute("Longitude");
		latitude = new Attribute("Latitude");

		// Initialize nominal attributes
		police_force = new Attribute("PoliceForce", this.policeForces);
		accident_severity = new Attribute("AccidentSeverity", this.accidentSeverities);
		day_of_week = new Attribute("DayOfWeek", this.daysOfWeek);
		local_authority = new Attribute("LocalAuthority", this.localAuthorities);
		first_road_class = new Attribute("FirstRoadClass", this.firstRoadClasses);
		speed_limit = new Attribute("SpeedLimit", this.speedLimits);
		light_conditions = new Attribute("LightConditions", this.lightConditions);
		weather_conditions = new Attribute("WeatherConditions", this.weatherConditions);
		urban_rural = new Attribute("UrbanRural", this.urbanRurals);
		police_officer_attend = new Attribute("DidPoliceOfficerAttendScene", this.policeOfficerAttends);

		ArrayList<Attribute> attributes = new ArrayList<>();
		attributes.add(longitude);
		attributes.add(latitude);
		attributes.add(police_force);
		attributes.add(accident_severity);
		attributes.add(day_of_week);
		attributes.add(local_authority);
		attributes.add(first_road_class);
		attributes.add(speed_limit);
		attributes.add(light_conditions);
		attributes.add(weather_conditions);
		attributes.add(urban_rural);
		attributes.add(police_officer_attend);

		dataset = new Instances("Accident Reporting in UK", attributes, 0);
		
		// Set class attribute
		dataset.setClass(police_officer_attend);
	}

	// Edit dataset population
	public void addInstance(double longitude, double latitude, String police_force, String accident_severity, String day_of_week,
			String local_authority, String first_road_class, String speed_limit, String light_conditions, String weather_conditions, String urban_rural, String police_officer_attend) {

		// Instance
		double[] values = new double[dataset.numAttributes()];

		values[0] = longitude;
		values[1] = latitude;
		values[2] = policeForces.indexOf(police_force);
		values[3] = accidentSeverities.indexOf(accident_severity);
		values[4] = daysOfWeek.indexOf(day_of_week);
		values[5] = localAuthorities.indexOf(local_authority);
		values[6] = firstRoadClasses.indexOf(first_road_class);
		values[7] = speedLimits.indexOf(speed_limit);
		values[8] = lightConditions.indexOf(light_conditions);
		values[9] = weatherConditions.indexOf(weather_conditions);
		values[10] = urbanRurals.indexOf(urban_rural);
		values[11] = policeOfficerAttends.indexOf(police_officer_attend);
		Instance instance = new DenseInstance(1.0, values);
		dataset.add(instance);
	}
        
        public Instance convertToInstance(double longitude, double latitude, String police_force, String accident_severity, String day_of_week,
			String local_authority, String first_road_class, String speed_limit, String light_conditions, String weather_conditions, String urban_rural) {

		// Instance
		double[] values = new double[dataset.numAttributes()];

		values[0] = longitude;
		values[1] = latitude;
		values[2] = policeForces.indexOf(police_force);
		values[3] = accidentSeverities.indexOf(accident_severity);
		values[4] = daysOfWeek.indexOf(day_of_week);
		values[5] = localAuthorities.indexOf(local_authority);
		values[6] = firstRoadClasses.indexOf(first_road_class);
		values[7] = speedLimits.indexOf(speed_limit);
		values[8] = lightConditions.indexOf(light_conditions);
		values[9] = weatherConditions.indexOf(weather_conditions);
		values[10] = urbanRurals.indexOf(urban_rural);
		//values[11] = policeOfficerAttends.indexOf(police_officer_attend);
		Instance instance = new DenseInstance(1.0, values);
		return instance;
	}
        
        public Instance convertToInstanceLongitudeLatitude(double longitude, double latitude){
            // Instance
            double[] values = new double[3];

            values[0] = longitude;
            values[1] = latitude;
            values[2] = 0.0;
            Instance instance = new DenseInstance(1.0, values);
            return instance;
        }

	public void copyInstanceIn(Instance instance) {
		dataset.add(instance);
	}

	public Instance getInstance(int index) {
		return dataset.get(index);
	}

	public void setDataset(Instances dataset) {
		this.dataset = dataset;
	}

	public Instances getDataset() {
		return this.dataset;
	}

	// Remove instance
	public void removeInstance(Instance instance) {
		dataset.remove(instance);
	}

	public List<Instance> getDatasetInstances() {
		return dataset;
	}

	public int getDatasetSize() {
		return dataset.size();
	}

	// Debug
	public void printDatasetInfo() {
		System.out.println("Dataset size: " + dataset.size());
		System.out.println("Dataset attributes: " + dataset.numAttributes());
	}

	// GET VALUE OF INSTANCE
	public double getLongitudeOf(int index) {
		return dataset.get(index).value(longitude);
	}

	public double getLatitudeOf(int index) {
		return dataset.get(index).value(latitude);
	}

	public String getPoliceForceOf(int index) {
		return policeForces.get((int) dataset.get(index).value(police_force));
	}
	
	public String getAccidentSeverityOf(int index) {
		return accidentSeverities.get((int) dataset.get(index).value(accident_severity));
	}

	public String getDayOfWeekOf(int index) {
		return daysOfWeek.get((int) dataset.get(index).value(day_of_week));
	}
	
	public String getLocalAuthorityOf(int index) {
		return localAuthorities.get((int) dataset.get(index).value(local_authority));
	}
	
	public String getFirstRoadClassOf(int index) {
		return firstRoadClasses.get((int) dataset.get(index).value(first_road_class));
	}
	
	public String getSpeedLimitOf(int index) {
		return speedLimits.get((int) dataset.get(index).value(speed_limit));
	}
	
	public String getLightConditionsOf(int index) {
		return lightConditions.get((int) dataset.get(index).value(light_conditions));
	}
	
	public String getWeatherConditionsOf(int index) {
		return weatherConditions.get((int) dataset.get(index).value(weather_conditions));
	}
	
	public String getUrbanRuralOf(int index) {
		return urbanRurals.get((int) dataset.get(index).value(urban_rural));
	}

	public String getDidPoliceOfferAttendOf(int index) {
		return policeOfficerAttends.get((int)dataset.get(index).value(police_officer_attend));
	}
	public boolean policeOfficerAttended(int index) {
		// 1: YES, 2: NO
		if(getDidPoliceOfferAttendOf(index).equals("1"))
			return true;
		return false;
	}
	
	// GET ARRAYLISTS
	public ArrayList<String> getPoliceForces() {
		return policeForces;
	}
	public ArrayList<String> getAccidentSeverities() {
		return accidentSeverities;
	}
	public ArrayList<String> getDaysOfWeek() {
		return daysOfWeek;
	}
	public ArrayList<String> getLocalAuthorities() {
		return localAuthorities;
	}
	public ArrayList<String> getFirstRoadClasses() {
		return firstRoadClasses;
	}
	public ArrayList<String> getSpeedLimits() {
		return speedLimits;
	}
	public ArrayList<String> getLightConditions() {
		return lightConditions;
	}
	public ArrayList<String> getWeatherConditions() {
		return weatherConditions;
	}
	public ArrayList<String> getUrbanRurals() {
		return urbanRurals;
	}
	public ArrayList<String> getPoliceOfficerAttends() {
		return policeOfficerAttends;
	}

	/*
	 * COPY DATASET
	 */
//	public Dataset copyDataset() {
//		// Initialize structure
//		Dataset newDataset = new Dataset();
//		newDataset.initializeDataset(lightConditions, policeForces, localAuthorities, pedestrianCrossingHumanControls,
//				pedestrianCrossingPhysicalFacilities, accidentSeverities, policeOfficerAttends);
//
//		// Copy instances in the new dataset
//		for (int i = 0; i < getDatasetSize(); i++) {
//			Instance instance = getInstance(i);
//			newDataset.copyInstanceIn(instance);
//		}
//
//		return newDataset;
//	}
//
//	public Dataset copyDatasetStructure() {
//		// Initialize structure
//		Dataset newDataset = new Dataset();
//		newDataset.initializeDataset(lightConditions, policeForces, localAuthorities, pedestrianCrossingHumanControls,
//				pedestrianCrossingPhysicalFacilities, accidentSeverities, policeOfficerAttends);
//		
//		return newDataset;
//	}
}

