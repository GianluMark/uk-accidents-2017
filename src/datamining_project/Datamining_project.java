/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining_project;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;
import weka.classifiers.misc.SerializedClassifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ArffLoader.ArffReader;
import weka.filters.Filter;
import weka.filters.supervised.attribute.NominalToBinary;
import weka.filters.unsupervised.attribute.OrdinalToNumeric;

/**
 *
 * @author gianl
 */
public class Datamining_project extends Application {

    // Parameters
    double PADDING_INSETS = 10.0;
    double BOX_SPACING = 5.0;

    // Interface variables
    HBox root;
    ScrollPane parametersScrollPane;
    VBox parametersVBox, resultsVBox;

    // Parameters interface variables
    Text long_lat_title;
    HBox long_lat_box;
    TextField longField, latField;
    Button checkCoordinatesButton;

    Text policeForceTitle;
    ComboBox policeForceComboBox;

    Text accidentSeverityTitle;
    ComboBox accidentSeverityComboBox;

    Text dayOfWeekTitle;
    ComboBox dayOfWeekComboBox;

    Text localAuthorityTitle;
    ComboBox localAuthorityComboBox;

    Text firstRoadClassTitle;
    ComboBox firstRoadClassComboBox;

    Text speedLimitTitle;
    ComboBox speedLimitComboBox;

    Text lightConditionsTitle;
    ComboBox lightConditionsComboBox;

    Text weatherConditionsTitle;
    ComboBox weatherConditionsComboBox;

    Text urbanRuralTitle;
    ComboBox urbanRuralComboBox;

    Button submitParametersButton;

    Text errorText;
    
    Text tips;

    // Results interface variables
    Text fullResultsTitle;
    Text fullResultsBody;

    Text policeForceResultsTitle;
    Text policeForceResultsBody;

    Text localAuthorityResultsTitle;
    Text localAuthorityResultsBody;

    Text predictedValue;

    // Nominal Feature Maps
    NominalFeatureMap policeForceMap, accidentSeverityMap, daysOfWeekMap, localAuthorityMap, firstRoadClassMap, speedLimitMap, lightConditionsMap, weatherConditionsMap, urbanRuralMap;

    // Dataset
    Dataset dataset;
    Instances policeForceDataset, localAuthorityDataset, filteredDataset;
    SerializedClassifier classifier, longLatPoliceForceClassifier, longLatLocalAuthorityClassifier;

    @Override
    public void start(Stage stage) {

        // Create interface
        root = new HBox();
        parametersScrollPane = new ScrollPane();
        parametersVBox = new VBox();
        resultsVBox = new VBox();

        // Populate interface
        root.getChildren().add(parametersScrollPane);
        parametersScrollPane.setContent(parametersVBox);
        root.getChildren().add(resultsVBox);

        // Colors
        resultsVBox.setBackground(new Background(new BackgroundFill(Color.GREY, null, null)));

        // Show interface
        Scene scene = new Scene(root, 300, 250);

        stage.setTitle("Data Mining Project");
        stage.setScene(scene);

        // Set window size
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX(primaryScreenBounds.getMinX());
        stage.setY(primaryScreenBounds.getMinY());
        stage.setWidth(primaryScreenBounds.getWidth());
        stage.setHeight(primaryScreenBounds.getHeight());

        // Set interface sizes
        parametersScrollPane.setPrefSize(stage.getWidth() / 2, stage.getHeight());
        parametersVBox.setPrefWidth(parametersScrollPane.getPrefWidth());
        resultsVBox.setPrefWidth(stage.getWidth() / 2);
        parametersVBox.setPadding(new Insets(PADDING_INSETS));
        resultsVBox.setPadding(new Insets(PADDING_INSETS));
        parametersVBox.setSpacing(BOX_SPACING);
        resultsVBox.setSpacing(BOX_SPACING);
        parametersInterface(parametersVBox);
        resultsInterface(resultsVBox);
        populateComboBoxs();
        triggers();
        initializeDataset();
        loadClassifier();

        //CLUSTERING
        //Clustering clustering = new Clustering(dataset);
        //clustering.calculateBestDBSCANValues();
        // Show GUI
        stage.show();

        System.out.println("Current Directory: " + Common.actualDirectory);
    }

    // PARAMETERS INTERFACE
    void parametersInterface(VBox box) {

        long_lat_title = new Text("Longitude and Latitude:");
        long_lat_box = new HBox();
        longField = new TextField();
        latField = new TextField();
        longField.setText("-0.80107");
        latField.setText("51.65006");
        checkCoordinatesButton = new Button("CHECK COORDINATES");

        policeForceTitle = new Text("Police Force:");
        policeForceComboBox = new ComboBox();

        accidentSeverityTitle = new Text("Accident Severity:");
        accidentSeverityComboBox = new ComboBox();

        dayOfWeekTitle = new Text("Day of week:");
        dayOfWeekComboBox = new ComboBox();

        localAuthorityTitle = new Text("Local Authority:");
        localAuthorityComboBox = new ComboBox();

        firstRoadClassTitle = new Text("First Road Class:");
        firstRoadClassComboBox = new ComboBox();

        speedLimitTitle = new Text("Speed Limit (MPH):");
        speedLimitComboBox = new ComboBox();

        lightConditionsTitle = new Text("Light Conditions at site:");
        lightConditionsComboBox = new ComboBox();

        weatherConditionsTitle = new Text("Weather Conditions at site:");
        weatherConditionsComboBox = new ComboBox();

        urbanRuralTitle = new Text("Urban or rural area:");
        urbanRuralComboBox = new ComboBox();

        submitParametersButton = new Button("SUBMIT");

        errorText = new Text("");
        
        tips = new Text("NOTES:\n> Longitude ranges from -7.41 to 1.76\n> Latitude ranges from 49.93 to 60.481");
        
        // Style
        long_lat_box.setSpacing(BOX_SPACING);
        submitParametersButton.setBackground(new Background(new BackgroundFill(Color.ORANGE, null, null)));
        checkCoordinatesButton.setBackground(new Background(new BackgroundFill(Color.ORANGE, null, null)));

        // Add elements to interface.
        box.getChildren().addAll(long_lat_title, long_lat_box);
        long_lat_box.getChildren().addAll(longField, latField, checkCoordinatesButton);
        box.getChildren().addAll(policeForceTitle, policeForceComboBox);
        box.getChildren().addAll(accidentSeverityTitle, accidentSeverityComboBox);
        box.getChildren().addAll(dayOfWeekTitle, dayOfWeekComboBox);
        box.getChildren().addAll(localAuthorityTitle, localAuthorityComboBox);
        box.getChildren().addAll(firstRoadClassTitle, firstRoadClassComboBox);
        box.getChildren().addAll(speedLimitTitle, speedLimitComboBox);
        box.getChildren().addAll(lightConditionsTitle, lightConditionsComboBox);
        box.getChildren().addAll(weatherConditionsTitle, weatherConditionsComboBox);
        box.getChildren().addAll(urbanRuralTitle, urbanRuralComboBox);
        box.getChildren().add(submitParametersButton);
        box.getChildren().add(errorText);
        box.getChildren().add(tips);
    }

    public void resultsInterface(VBox box) {
        fullResultsTitle = new Text("Number of reported and not reported accidents with these parameters in DB:");
        fullResultsBody = new Text("");

        policeForceResultsTitle = new Text("");
        policeForceResultsBody = new Text("");

        localAuthorityResultsTitle = new Text("");
        localAuthorityResultsBody = new Text("");

        predictedValue = new Text("");

        // Style
        fullResultsTitle.setStyle("-fx-font-weight: bold;");
        policeForceResultsTitle.setStyle("-fx-font-weight: bold;");
        localAuthorityResultsTitle.setStyle("-fx-font-weight: bold;");
        predictedValue.setStyle("-fx-font-weight: bold;");

        // Add elements to interface.
        box.getChildren().addAll(fullResultsTitle, fullResultsBody, policeForceResultsTitle, policeForceResultsBody, localAuthorityResultsTitle, localAuthorityResultsBody, predictedValue);
    }

    public void populateComboBoxs() {

        // Instantiate faeture maps
        policeForceMap = new NominalFeatureMap();
        accidentSeverityMap = new NominalFeatureMap();
        daysOfWeekMap = new NominalFeatureMap();
        localAuthorityMap = new NominalFeatureMap();
        firstRoadClassMap = new NominalFeatureMap();
        speedLimitMap = new NominalFeatureMap();
        lightConditionsMap = new NominalFeatureMap();
        weatherConditionsMap = new NominalFeatureMap();
        urbanRuralMap = new NominalFeatureMap();

        // POLICE FORCES
        ArrayList<String> policeForces = readFeaturesFromFile(Common.policeForceFeatures);
        for (int i = 0; i < policeForces.size(); i++) {
            String[] values = policeForces.get(i).split("\t");
            for (int j = 0; j < Common.policeForceIndexValues.length; j++) {
                if (Integer.toString(Common.policeForceIndexValues[j]).equals(values[0])) {
                    policeForceMap.addValue(values[0], values[1]);
                    policeForceComboBox.getItems().add(values[1]);
                }
            }
        }

        // ACCIDENT SEVERITY
        ArrayList<String> accidentSevs = readFeaturesFromFile(Common.accidentSeverityFeatures);
        for (int i = 0; i < accidentSevs.size(); i++) {
            String[] values = accidentSevs.get(i).split("\t");
            accidentSeverityMap.addValue(values[0], values[1]);
            accidentSeverityComboBox.getItems().add(values[1]);
        }

        // DAYS OF WEEK
        ArrayList<String> days = readFeaturesFromFile(Common.dayOfWeekFeatures);
        for (int i = 0; i < days.size(); i++) {
            String[] values = days.get(i).split("\t");
            daysOfWeekMap.addValue(values[0], values[1]);
            dayOfWeekComboBox.getItems().add(values[1]);
        }

        // LOCAL AUTHORITY
        ArrayList<String> localAuthorities = readFeaturesFromFile(Common.localAuthorityFeatures);
        for (int i = 0; i < localAuthorities.size(); i++) {
            String[] values = localAuthorities.get(i).split("\t");
            for (int j = 0; j < Common.localAuthorityIndexValues.length; j++) {
                if (Integer.toString(Common.localAuthorityIndexValues[j]).equals(values[0])) {
                    localAuthorityMap.addValue(values[0], values[1]);
                    localAuthorityComboBox.getItems().add(values[1]);
                }
            }
        }

        // FIRST ROAD CLASS
        ArrayList<String> firstRoadClasses = readFeaturesFromFile(Common.firstRoadClassFeatures);
        for (int i = 0; i < firstRoadClasses.size(); i++) {
            String[] values = firstRoadClasses.get(i).split("\t");
            firstRoadClassMap.addValue(values[0], values[1]);
            firstRoadClassComboBox.getItems().add(values[1]);
        }

        // SPEED LIMIT
        ArrayList<String> speedLimits = readFeaturesFromFile(Common.speedLimitFeatures);
        for (int i = 0; i < speedLimits.size(); i++) {
            String[] values = speedLimits.get(i).split("\t");
            speedLimitMap.addValue(values[0], values[1]);
            speedLimitComboBox.getItems().add(values[1]);
        }

        // LIGHT CONDITIONS
        ArrayList<String> lightConditions = readFeaturesFromFile(Common.lightConditionsFeatures);
        for (int i = 0; i < lightConditions.size(); i++) {
            String[] values = lightConditions.get(i).split("\t");
            lightConditionsMap.addValue(values[0], values[1]);
            lightConditionsComboBox.getItems().add(values[1]);
        }

        // WEATHER CONDITIONS
        ArrayList<String> weatherConditions = readFeaturesFromFile(Common.weatherConditionsFeatures);
        for (int i = 0; i < weatherConditions.size(); i++) {
            String[] values = weatherConditions.get(i).split("\t");
            weatherConditionsMap.addValue(values[0], values[1]);
            weatherConditionsComboBox.getItems().add(values[1]);
        }

        // URBAN OR RURAL
        ArrayList<String> urbalRurals = readFeaturesFromFile(Common.urbanRuralFeatures);
        for (int i = 0; i < urbalRurals.size(); i++) {
            String[] values = urbalRurals.get(i).split("\t");
            urbanRuralMap.addValue(values[0], values[1]);
            urbanRuralComboBox.getItems().add(values[1]);
        }
    }

    public void triggers() {

        checkCoordinatesButton.setOnAction((ActionEvent event) -> {

            if (longField.getText().isEmpty() || latField.getText().isEmpty()) {
                setErrorText("Insert coordinates");
                return;
            }

            if (!checkLongLatFormat(longField.getText())) {
                setErrorText("Use correct format for longitude");
                return;
            }

            if (!checkLongLatFormat(latField.getText())) {
                setErrorText("Use correct format for latitude");
                return;
            }
            
            if (Double.parseDouble(longField.getText()) < Common.longitudeMin || Double.parseDouble(longField.getText()) > Common.longitudeMax) {
                setErrorText("Longitude out of range");
                return;
            }

            if (Double.parseDouble(latField.getText()) < Common.latitudeMin || Double.parseDouble(latField.getText()) > Common.latitudeMax) {
                setErrorText("Latitude out of range");
                return;
            }

            // PREDICT POLICE FORCE
            double normalizedLongitude, normalizedLatitude;

            normalizedLongitude = normalizeValue(Double.parseDouble(longField.getText()), Common.longitudeMin, Common.longitudeMax);
            normalizedLatitude = normalizeValue(Double.parseDouble(latField.getText()), Common.latitudeMin, Common.latitudeMax);

            Instance instance = dataset.convertToInstanceLongitudeLatitude(normalizedLongitude, normalizedLatitude);
            policeForceDataset.add(instance);
            instance.setDataset(policeForceDataset);
            double result;
            try {
                result = longLatPoliceForceClassifier.classifyInstance(instance);

                int policeForceIndex = (int) result;
                policeForceComboBox.getSelectionModel().select(policeForceIndex);
            } catch (Exception e) {
                System.err.println("[POLICE FORCE CLASSIFICATION EXCEPTION] " + e.toString());
            }

            // PREDICT LOCAL AUTHORITY
            instance = dataset.convertToInstanceLongitudeLatitude(normalizedLongitude, normalizedLatitude);
            localAuthorityDataset.add(instance);
            instance.setDataset(localAuthorityDataset);
            try {
                result = longLatLocalAuthorityClassifier.classifyInstance(instance);

                int localAuthorityIndex = (int) result;
                localAuthorityComboBox.getSelectionModel().select(localAuthorityIndex);
            } catch (Exception e) {
                System.err.println("[LOCAL AUTHORITY CLASSIFICATION EXCEPTION] " + e.toString());
            }

        });

        submitParametersButton.setOnAction((ActionEvent event) -> {

            if (longField.getText().isEmpty() || latField.getText().isEmpty()) {
                setErrorText("Insert coordinates");
                return;
            }

            if (!checkLongLatFormat(longField.getText())) {
                setErrorText("Use correct format for longitude");
                return;
            }

            if (!checkLongLatFormat(latField.getText())) {
                setErrorText("Use correct format for latitude");
                return;
            }
            
            if (Double.parseDouble(longField.getText()) < Common.longitudeMin || Double.parseDouble(longField.getText()) > Common.longitudeMax) {
                setErrorText("Longitude out of range");
                return;
            }

            if (Double.parseDouble(latField.getText()) < Common.latitudeMin || Double.parseDouble(latField.getText()) > Common.latitudeMax) {
                setErrorText("Latitude out of range");
                return;
            }

            if (policeForceComboBox.getSelectionModel().isEmpty() || accidentSeverityComboBox.getSelectionModel().isEmpty() || dayOfWeekComboBox.getSelectionModel().isEmpty() || localAuthorityComboBox.getSelectionModel().isEmpty() || firstRoadClassComboBox.getSelectionModel().isEmpty() || speedLimitComboBox.getSelectionModel().isEmpty() || lightConditionsComboBox.getSelectionModel().isEmpty() || weatherConditionsComboBox.getSelectionModel().isEmpty() || urbanRuralComboBox.getSelectionModel().isEmpty()) {
                setErrorText("Fill fields");
                return;
            }

            clearErrorText();

            // GET STATS
            // Get values from interface
            String longitude, latitude, policeForce, accidentSeverity, dayOfWeek, localAuthority, firstRoadClass, speedLimit, lightConditions, weatherConditions, urbanRural;
            longitude = longField.getText();
            latitude = latField.getText();
            policeForce = policeForceMap.getIndexFromName(policeForceComboBox.getValue().toString());
            accidentSeverity = accidentSeverityMap.getIndexFromName(accidentSeverityComboBox.getValue().toString());
            dayOfWeek = daysOfWeekMap.getIndexFromName(dayOfWeekComboBox.getValue().toString());
            localAuthority = localAuthorityMap.getIndexFromName(localAuthorityComboBox.getValue().toString());
            firstRoadClass = firstRoadClassMap.getIndexFromName(firstRoadClassComboBox.getValue().toString());
            speedLimit = speedLimitMap.getIndexFromName(speedLimitComboBox.getValue().toString());
            lightConditions = lightConditionsMap.getIndexFromName(lightConditionsComboBox.getValue().toString());
            weatherConditions = weatherConditionsMap.getIndexFromName(weatherConditionsComboBox.getValue().toString());
            urbanRural = urbanRuralMap.getIndexFromName(urbanRuralComboBox.getValue().toString());

            // Search for values in dataset
            int yesValues = 0, noValues = 0, yesValuesPoliceForce = 0, noValuesPoliceForce = 0, yesValuesLocalAuthority = 0, noValuesLocalAuthority = 0;

            for (int i = 0; i < dataset.getDatasetSize(); i++) {

                if (policeForce.equals(dataset.getPoliceForceOf(i)) && accidentSeverity.equals(dataset.getAccidentSeverityOf(i)) && dayOfWeek.equals(dataset.getDayOfWeekOf(i)) && localAuthority.equals(dataset.getLocalAuthorityOf(i)) && firstRoadClass.equals(dataset.getFirstRoadClassOf(i)) && speedLimit.equals(dataset.getSpeedLimitOf(i)) && lightConditions.equals(dataset.getLightConditionsOf(i)) && weatherConditions.equals(dataset.getWeatherConditionsOf(i)) && urbanRural.equals(dataset.getUrbanRuralOf(i))) {
                    // If object is found
                    if (dataset.getDidPoliceOfferAttendOf(i).equals(Common.yesValue)) {
                        yesValues++;
                    } else {
                        noValues++;
                    }
                }

                if (policeForce.equals(dataset.getPoliceForceOf(i))) {
                    if (dataset.getDidPoliceOfferAttendOf(i).equals(Common.yesValue)) {
                        yesValuesPoliceForce++;
                    } else {
                        noValuesPoliceForce++;
                    }
                }

                if (localAuthority.equals(dataset.getLocalAuthorityOf(i))) {
                    if (dataset.getDidPoliceOfferAttendOf(i).equals(Common.yesValue)) {
                        yesValuesLocalAuthority++;
                    } else {
                        noValuesLocalAuthority++;
                    }
                }
            }

            writeResults(yesValues, noValues, yesValuesPoliceForce, noValuesPoliceForce, yesValuesLocalAuthority, noValuesLocalAuthority);

            // Filter data
            double[] values = new double[filteredDataset.numAttributes()];
            int counter = 0;
            double filteredLongitude = normalizeValue(Double.parseDouble(longField.getText()), Common.longitudeMin, Common.longitudeMax);
            values[counter] = filteredLongitude;
            counter++;
            double filteredLatitude = normalizeValue(Double.parseDouble(latField.getText()), Common.latitudeMin, Common.latitudeMax);
            values[counter] = filteredLatitude;
            counter++;
            double[] policeForceVector = new double[Common.numberOfPoliceForces];
            for (int i = 0; i < policeForceVector.length; i++) {
                if (i == policeForceMap.getPositionFromList(policeForce)) {
                    policeForceVector[i] = 1;
                } else {
                    policeForceVector[i] = 0;
                }
                values[counter] = policeForceVector[i];
                counter++;
            }
            double filteredAccidentSeverity = Double.parseDouble(accidentSeverity);
            filteredAccidentSeverity--;
            filteredAccidentSeverity = normalizeValue(filteredAccidentSeverity, Common.accidentSeverityMin, Common.accidentSeverityMax);
            values[counter] = filteredAccidentSeverity;
            counter++;
            double filteredDayOfWeek = Double.parseDouble(dayOfWeek);
            filteredDayOfWeek--;
            filteredDayOfWeek = normalizeValue(filteredDayOfWeek, Common.dayOfWeekMin, Common.dayOfWeekMax);
            values[counter] = filteredDayOfWeek;
            counter++;
            double[] localAuthorityVector = new double[Common.numberOfLocalAuthorities];
            for (int i = 0; i < localAuthorityVector.length; i++) {
                if (i == localAuthorityMap.getPositionFromList(localAuthority)) {
                    localAuthorityVector[i] = 1;
                } else {
                    localAuthorityVector[i] = 0;
                }
                values[counter] = localAuthorityVector[i];
                counter++;
            }
            double filteredFirstRoadOfClass = Double.parseDouble(firstRoadClass);
            filteredFirstRoadOfClass--;
            filteredFirstRoadOfClass = normalizeValue(filteredFirstRoadOfClass, Common.firstRoadClassMin, Common.firstRoadClassMax);
            values[counter] = filteredFirstRoadOfClass;
            counter++;
            double filteredSpeedLimit = 0;
            if (speedLimit.equals("20")) {
                filteredSpeedLimit = 0;
            } else if (speedLimit.equals("30")) {
                filteredSpeedLimit = 1;
            } else if (speedLimit.equals("40")) {
                filteredSpeedLimit = 2;
            } else if (speedLimit.equals("50")) {
                filteredSpeedLimit = 3;
            } else if (speedLimit.equals("60")) {
                filteredSpeedLimit = 4;
            } else if (speedLimit.equals("70")) {
                filteredSpeedLimit = 5;
            }
            filteredSpeedLimit = normalizeValue(filteredSpeedLimit, Common.speedLimitMin, Common.speedLimitMax);
            values[counter] = filteredSpeedLimit;
            counter++;
            double[] lightConditionsVector = new double[Common.numberOfLightConditions];
            for (int i = 0; i < lightConditionsVector.length; i++) {
                if (i == lightConditionsMap.getPositionFromList(lightConditions)) {
                    lightConditionsVector[i] = 1;
                } else {
                    lightConditionsVector[i] = 0;
                }
                values[counter] = lightConditionsVector[i];
                counter++;
            }
            double[] weatherConditionsVector = new double[Common.numberOfWeatherConditions];
            for (int i = 0; i < weatherConditionsVector.length; i++) {
                if (i == weatherConditionsMap.getPositionFromList(weatherConditions)) {
                    weatherConditionsVector[i] = 1;
                } else {
                    weatherConditionsVector[i] = 0;
                }
                values[counter] = weatherConditionsVector[i];
                counter++;
            }
            values[counter] = urbanRuralMap.indexes.indexOf(urbanRural);;
            counter++;

            Instance filteredInstance = new DenseInstance(1.0, values);
            filteredDataset.add(filteredInstance);
            filteredInstance.setDataset(filteredDataset);

            double result = 0.0;
            try {
                result = classifier.classifyInstance(filteredInstance);
                double[] prediction = classifier.distributionForInstance(filteredInstance);
                if (result == 0.0) // NO
                {
                    writePredictedResult("NO", prediction[1], prediction[0]);
                } else {
                    writePredictedResult("YES", prediction[1], prediction[0]);
                }

                for (int i = 0; i < prediction.length; i++) {
                    System.out.print(prediction[i] + "\t");
                }
                System.out.println(result + "\n");
                //get the predicted probabilities 
            } catch (Exception e) {
                System.err.println("[CLASSIFICATION EXCEPTION] " + e.toString());
            }
        });
    }

    // DATASET
    public void initializeDataset() {

        // MAIN DATASET
        ArrayList<String> classValues = new ArrayList<>();
        classValues.add("2");
        classValues.add("1");
        dataset = new Dataset();
        dataset.initializeDataset(policeForceMap.getIndexes(), accidentSeverityMap.getIndexes(), daysOfWeekMap.getIndexes(), localAuthorityMap.getIndexes(), firstRoadClassMap.getIndexes(), speedLimitMap.getIndexes(), lightConditionsMap.getIndexes(), weatherConditionsMap.getIndexes(), urbanRuralMap.getIndexes(), classValues);

        BufferedReader reader;
        Instances data = null;
        try {
            reader = new BufferedReader(new FileReader(Common.datasetFile));

            ArffReader arff = new ArffReader(reader);
            data = arff.getData();
            data.setClassIndex(data.numAttributes() - 1);
        } catch (Exception e) {
            System.err.println("[ARFF LOADER EXCEPTION] " + e.toString());
        }

        for (int i = 0; i < data.size(); i++) {
            dataset.copyInstanceIn(data.instance(i));
        }

        // LONGITUDE LATITUDE DATASET
        Attribute longitude = new Attribute("Longitude");
        Attribute latitude = new Attribute("Latitude");
        Attribute policeForceValues = new Attribute("PoliceForce", policeForceMap.getIndexes());
        Attribute localAuthorityValues = new Attribute("LocalAuthority", localAuthorityMap.getIndexes());

        ArrayList<Attribute> policeForceAttributes = new ArrayList<>();
        policeForceAttributes.add(longitude);
        policeForceAttributes.add(latitude);
        policeForceAttributes.add(policeForceValues);
        ArrayList<Attribute> localAuthorityAttributes = new ArrayList<>();
        localAuthorityAttributes.add(longitude);
        localAuthorityAttributes.add(latitude);
        localAuthorityAttributes.add(localAuthorityValues);

        policeForceDataset = new Instances("Accident Reporting in UK", policeForceAttributes, 0);
        localAuthorityDataset = new Instances("Accident Reporting in UK", localAuthorityAttributes, 0);
        policeForceDataset.setClass(policeForceValues);
        localAuthorityDataset.setClass(localAuthorityValues);

        // TEMPORARY DATASET FOR FILTERED DATASET
        ArrayList<Attribute> filteredDatasetAttributes = new ArrayList<>();

        Attribute filteredLongitude = new Attribute("Longitude");
        Attribute filteredLatitude = new Attribute("Latitude");
        filteredDatasetAttributes.add(filteredLongitude);
        filteredDatasetAttributes.add(filteredLatitude);

        ArrayList<Attribute> policeForceBinaryAttributeList = new ArrayList<>();
        for (int i = 0; i < Common.numberOfPoliceForces; i++) {
            Attribute policeForceBinary = new Attribute("PoliceForce=" + policeForceMap.indexes.get(i));
            policeForceBinaryAttributeList.add(policeForceBinary);
            filteredDatasetAttributes.add(policeForceBinary);
        }

        Attribute accidentSeverityAttribute = new Attribute("AccidentSeverity");
        Attribute dayOfWeekAttribute = new Attribute("DayOfWeek");
        filteredDatasetAttributes.add(accidentSeverityAttribute);
        filteredDatasetAttributes.add(dayOfWeekAttribute);

        ArrayList<Attribute> localAuthorityBinaryAttributeList = new ArrayList<>();
        for (int i = 0; i < Common.numberOfLocalAuthorities; i++) {
            Attribute localAuthorityBinary = new Attribute("LocalAuthority=" + localAuthorityMap.indexes.get(i));
            localAuthorityBinaryAttributeList.add(localAuthorityBinary);
            filteredDatasetAttributes.add(localAuthorityBinary);
        }

        Attribute firstRoadClassAttribute = new Attribute("FirstRoadClass");
        Attribute speedLimitAttribute = new Attribute("SpeedLimit");
        filteredDatasetAttributes.add(firstRoadClassAttribute);
        filteredDatasetAttributes.add(speedLimitAttribute);

        ArrayList<Attribute> lightConditionsBinaryAttributeList = new ArrayList<>();
        for (int i = 0; i < Common.numberOfLightConditions; i++) {
            Attribute lightConditionsBinary = new Attribute("LightConditions=" + lightConditionsMap.indexes.get(i));
            lightConditionsBinaryAttributeList.add(lightConditionsBinary);
            filteredDatasetAttributes.add(lightConditionsBinary);
        }

        ArrayList<Attribute> weatherConditionsBinaryAttributeList = new ArrayList<>();
        for (int i = 0; i < Common.numberOfWeatherConditions; i++) {
            Attribute weatherConditionsBinary = new Attribute("WeatherConditions=" + weatherConditionsMap.indexes.get(i));
            weatherConditionsBinaryAttributeList.add(weatherConditionsBinary);
            filteredDatasetAttributes.add(weatherConditionsBinary);
        }

        Attribute urbanRuralAttribute = new Attribute("UrbanRural", urbanRuralMap.getIndexes());
        Attribute didPoliceOfficerAttendedAttribute = new Attribute("DidPoliceOfficerAttendScene", classValues);
        filteredDatasetAttributes.add(urbanRuralAttribute);
        filteredDatasetAttributes.add(didPoliceOfficerAttendedAttribute);

        filteredDataset = new Instances("Filtered Dataset", filteredDatasetAttributes, 0);
        filteredDataset.setClass(didPoliceOfficerAttendedAttribute);

    }

    public void loadClassifier() {

        // LOAD CLASSIFIER LONG LAT TO POLICE FORCE
        try {
            longLatPoliceForceClassifier = new SerializedClassifier();
            longLatPoliceForceClassifier.setModelFile(new File(Common.longLatPoliceForceModel));
        } catch (Exception e) {
            System.err.println("[CLASSIFIER MODEL LONG LAT POLICE FORCE LOAD EXCEPTION] " + e.toString());
        }

        // LOAD CLASSIFIER LONG LAT TO LOCAL AUTHORITY
        try {
            longLatLocalAuthorityClassifier = new SerializedClassifier();
            longLatLocalAuthorityClassifier.setModelFile(new File(Common.longLatLocalAuthorityModel));
        } catch (Exception e) {
            System.err.println("[CLASSIFIER MODEL LONG LAT LOCAL AUTHORITY LOAD EXCEPTION] " + e.toString());
        }

        // LOAD MAIN CLASSIFIER
        try {
            classifier = new SerializedClassifier();
            classifier.setModelFile(new File(Common.classifierModel));
        } catch (Exception e) {
            System.err.println("[CLASSIFIER MODEL LOAD EXCEPTION] " + e.toString());
        }
    }

    // TOOLS
    public ArrayList<String> readFeaturesFromFile(String filePath) {

        ArrayList<String> values = new ArrayList<>();

        BufferedReader objReader = null;
        try {
            String strCurrentLine;
            objReader = new BufferedReader(new FileReader(filePath));

            while ((strCurrentLine = objReader.readLine()) != null) {
                values.add(strCurrentLine);
            }
        } catch (IOException e) {
            System.err.println("[READ FILE EXCEPTION] " + e.toString());

        } finally {

            try {
                if (objReader != null) {
                    objReader.close();
                }
            } catch (IOException e) {
                System.err.println("[CLOSE FILE READER EXCEPTION] " + e.toString());
            }
        }
        return values;

    }

    public double normalizeValue(double value, double min, double max) {

        double normalizedValue = Common.newMin + (value - min) * (Common.newMax - Common.newMin) / (max - min);
        return normalizedValue;
    }

    public boolean checkLongLatFormat(String value) {
        final String Digits = "(\\p{Digit}+)";
        final String HexDigits = "(\\p{XDigit}+)";
        // an exponent is 'e' or 'E' followed by an optionally
        // signed decimal integer.
        final String Exp = "[eE][+-]?" + Digits;
        final String fpRegex
                = ("[\\x00-\\x20]*"
                + // Optional leading "whitespace"
                "[+-]?("
                + // Optional sign character
                "NaN|"
                + // "NaN" string
                "Infinity|"
                + // "Infinity" string
                // A decimal floating-point string representing a finite positive
                // number without a leading sign has at most five basic pieces:
                // Digits . Digits ExponentPart FloatTypeSuffix
                //
                // Since this method allows integer-only strings as input
                // in addition to strings of floating-point literals, the
                // two sub-patterns below are simplifications of the grammar
                // productions from section 3.10.2 of
                // The Java Language Specification.
                // Digits ._opt Digits_opt ExponentPart_opt FloatTypeSuffix_opt
                "(((" + Digits + "(\\.)?(" + Digits + "?)(" + Exp + ")?)|"
                + // . Digits ExponentPart_opt FloatTypeSuffix_opt
                "(\\.(" + Digits + ")(" + Exp + ")?)|"
                + // Hexadecimal strings
                "(("
                + // 0[xX] HexDigits ._opt BinaryExponent FloatTypeSuffix_opt
                "(0[xX]" + HexDigits + "(\\.)?)|"
                + // 0[xX] HexDigits_opt . HexDigits BinaryExponent FloatTypeSuffix_opt
                "(0[xX]" + HexDigits + "?(\\.)" + HexDigits + ")"
                + ")[pP][+-]?" + Digits + "))"
                + "[fFdD]?))"
                + "[\\x00-\\x20]*");// Optional trailing "whitespace"
        return Pattern.matches(fpRegex, value); // Double.valueOf(value); // Will not throw NumberFormatException
    }

    public void setErrorText(String value) {
        errorText.setText(value);
    }

    public void clearErrorText() {
        errorText.setText("");
    }

    // RESULTS INTERFACE
    public void writeResults(int fullYes, int fullNo, int policeForceYes, int policeForceNo, int localAuthorityYes, int localAuthorityNo) {

        fullResultsBody.setText("YES: " + fullYes + "\nNO: " + fullNo + "\n");

        policeForceResultsTitle.setText("Reported and not reported accidents in DB for Police Force: " + policeForceComboBox.getValue().toString());
        policeForceResultsBody.setText("YES: " + policeForceYes + "\nNO: " + policeForceNo + "\n");

        localAuthorityResultsTitle.setText("Reported and not reported accidents in DB for Local Authority: " + localAuthorityComboBox.getValue().toString());
        localAuthorityResultsBody.setText("YES: " + localAuthorityYes + "\nNO: " + localAuthorityNo + "\n");
    }

    public void writePredictedResult(String result, double probabilityYES, double probabilityNO) {
        predictedValue.setText("Result of classification: " + result + "\n\nProbability of YES: " + probabilityYES + "\nProbability of NO: " + probabilityNO);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
